from simple_exchange import SimpleExchange

class TestSimpleExchange:
    def test_constructor_1(self):
        simpleExchange = SimpleExchange([], {}, {})
        assert simpleExchange is not None
    
    def test_constructor_2(self):
        try:
            assets = ["ADA", "USD"]
            prices = {"ADA":{"BUSD":2.0}}
            accounts = {}
            _ = SimpleExchange(assets, prices, accounts)
            raise Exception
        except AssertionError:
            pass
    
        try:
            assets = ["ADA", "BUSD"]
            prices = {"BTC":{"BUSD":2.0}}
            accounts = {}
            _ = SimpleExchange(assets, prices, accounts)
            raise Exception
        except AssertionError:
            pass
    
        try:
            assets = ["ADA", "USD"]
            prices = {"ADA":{"USD":2.0}}
            accounts = {"testAccount":{"BTC":250, "BUSD":500}}
            _ = SimpleExchange(assets, prices, accounts)
            raise Exception
        except AssertionError:
            pass

    def test_getAllPrices_1(self):
        assets = ["ADA", "BUSD"]
        prices = {"ADA":{"BUSD":2.0}}
        accounts = {}
        simpleExchange = SimpleExchange(assets, prices, accounts)
        assert simpleExchange.getAllPrices() == prices

    def test_getAccountBalance_1(self):
        assets = ["ADA", "BUSD"]
        prices = {"ADA":{"BUSD":2.0}}
        accounts = {"testAccount":{"ADA":250, "BUSD":500}}
        simpleExchange = SimpleExchange(assets, prices, accounts)
        assert simpleExchange.getAccountBalance("testAccount") == accounts["testAccount"]
    
    def test_getPrice_1(self):
        assets = ["ADA", "BUSD", "BTC"]
        prices = {"ADA":{"BUSD":2.0}, "BTC":{"BUSD":50000, "ADA":25000}}
        accounts = {}
        simpleExchange = SimpleExchange(assets, prices, accounts)
        assert simpleExchange.getPrice("ADA", "BUSD") == 2.0
        assert simpleExchange.getPrice("BUSD", "ADA") == 0.5

        assert simpleExchange.getPrice("BTC", "BUSD") == 50000
        assert simpleExchange.getPrice("BUSD", "BTC") == 0.00002

        assert simpleExchange.getPrice("BTC", "ADA") == 25000
        assert simpleExchange.getPrice("ADA", "BTC") == 0.00004

        try:
            _ = simpleExchange.getPrice("ADA", "USDC")
            raise Exception
        except AssertionError:
            pass
        
    def test_swap_1(self):
        assets = ["ADA", "BUSD"]
        prices = {"ADA":{"BUSD":2.0}}
        accounts = {"testAccount":{"ADA":250, "BUSD":500}}
        simpleExchange = SimpleExchange(assets, prices, accounts)
        
        assert simpleExchange.getAccountBalance("testAccount") == {"ADA":250, "BUSD":500}
        simpleExchange.swap("testAccount", "ADA", "BUSD", 250)
        assert simpleExchange.getAccountBalance("testAccount") == {"BUSD":1000}
        simpleExchange.swap("testAccount", "BUSD", "ADA", 500)
        assert simpleExchange.getAccountBalance("testAccount") == {"ADA":250, "BUSD":500}

    def test_swap_2(self):
        assets = ["ADA", "BUSD"]
        prices = {"ADA":{"BUSD":2.0}}
        accounts = {"testAccount":{"ADA":250, "BUSD":500}}
        simpleExchange = SimpleExchange(assets, prices, accounts)

        try:
            simpleExchange.swap("testAccount", "ADA", "BUSD", 300)
            raise Exception
        except AssertionError:
            pass

    def test_swap_3(self):
        assets = ["ADA", "BUSD"]
        prices = {"ADA":{"BUSD":2.0}}
        accounts = {"testAccount":{"ADA":250, "BUSD":500}}
        simpleExchange = SimpleExchange(assets, prices, accounts)

        try:
            simpleExchange.swap("testAccount", "BTC", "BUSD", 300)
            raise Exception
        except AssertionError:
            pass
