from typing import Dict, List

class SimpleExchange:
    def __init__(self, assets:List[str], prices:Dict[str, Dict[str, float]], accounts:Dict[str, Dict[str, float]]):
        self.assets = assets
        for asset1 in prices.keys():
            assert asset1 in assets, f"Asset '{asset1}' not in {self.assets}"
            for asset2 in prices[asset1]:
                assert asset2 in assets, f"Asset '{asset2}' not in {self.assets}"
        self.prices = prices
        for account in accounts.keys():
            for asset in accounts[account]:
                assert asset in self.assets, f"Asset '{asset}' of account '{account}' not in {self.assets}"
        self.accounts = accounts

    def getAllPrices(self) -> Dict[str, float]:
        return self.prices

    def getAccountBalance(self, account:str) -> Dict[str, float]:
        return self.accounts[account]

    def getPrice(self, token1:str, token2:str) -> float:
        prices = self.getAllPrices()
        assert (token1 in prices and token2 in prices[token1]) or \
            (token2 in prices and token1 in prices[token2])
        try:
            return prices[token1][token2]
        except:
            return 1 / prices[token2][token1]

    def swap(self, account:str, token1:str, token2:str, value1:float):
        Repr = str(self).split(" ")[0].split(".")[-1]
        price = self.getPrice(token1, token2)

        addedValue2 = price * value1
        print(f"[{Repr}] Swapping {value1} * {token1} to {token2} at rate {price}, resulting {addedValue2} {token2}")
        # ATOMICLULZ
        account = self.getAccountBalance(account)
        assert value1 <= account[token1]
        account[token1] -= value1
        if account[token1] < 1e-5:
            del account[token1]
        if not token2 in account:
            account[token2] = 0
        account[token2] += addedValue2
